using System;
using Base;
using UnityEngine;

namespace UI.HUD
{
    public class HUDTopMediator : BaseHudMediator<HUDTopView, BaseHudData>
    {
        private Action OnOpenLevelUpPopup;

        public override void Mediate(BaseHudView value, GameManagersContexts managers)// Start
        {
            base.Mediate(value, managers);

            _managers.GameStateManager.STATE_CHANGED += GameStateChanged;
            _managers.ProfilePlayerManager.OnPlayerEarnExperience += PlayerTryAddLevel;
            _managers.ProfilePlayerManager.OnPlayerNameChanged += PlayerNameChanged;

            Target.PlayerInfoButton.onClick.AddListener(OnUserInfoOnUserButton);
            Target.FreeCoinsButtonButton.onClick.AddListener(OnFreeCoinsButton);
            Target.SettingsButton.onClick.AddListener(OnSettingsButton);
            Target.ShopButton.onClick.AddListener(OnShopButton);
        }

        public override void UnMediate()// OnDestroy
        {
            base.UnMediate();

            _managers.GameStateManager.STATE_CHANGED -= GameStateChanged;
            _managers.ProfilePlayerManager.OnPlayerEarnExperience -= PlayerTryAddLevel;
            _managers.ProfilePlayerManager.OnPlayerNameChanged -= PlayerNameChanged;

            Target.PlayerInfoButton.onClick.RemoveListener(OnUserInfoOnUserButton);
            Target.FreeCoinsButtonButton.onClick.RemoveListener(OnFreeCoinsButton);
            Target.SettingsButton.onClick.RemoveListener(OnSettingsButton);
            Target.ShopButton.onClick.RemoveListener(OnShopButton);
        }

        protected override void ShowStart() // OnEnable
        {
            base.ShowStart();

            Target.LevelPlayerText.text = _managers.ProfilePlayerManager.Level.ToString();
            Target.NamePlayerText.text = _managers.ProfilePlayerManager.Name;

            OnOpenLevelUpPopup?.SafeInvoke();
        }

        private void PlayerNameChanged(string name)
        {
            Target.NamePlayerText.text = name;
        }


        private void PlayerTryAddLevel(int experience)
        {
            _managers.LevelPlayerManager.TryAddLevel(experience);
        }

        public void GameStateChanged(GameState state)
        {
            Target.ShopActiveObj.SetActive(state == GameState.Shop);
        }

        public void OnUserInfoOnUserButton()
        {
            _managers.WindowsManager.Open(WindowType.UserInfo);
        }

        public void OnFreeCoinsButton()
        {
            Debug.Log("OnFreeCoinsButton");
        }

        public void OnSettingsButton()
        {
            _managers.WindowsManager.Open(WindowType.SettingsLobby);
        }

        public void OnShopButton()
        {
            _managers.GameStateManager.Current = GameState.Shop;
        }

    }
}
