using UnityEngine.UI;
using UnityEngine;

namespace UI.HUD
{
    public class HUDTopView : BaseHudView
    {
        [Header("Buttons")]
        [SerializeField] private Button _playerInfoButton;
        [SerializeField] private Button _freeCoinsButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _shopButton;

        [Space]

        [Header("Texts")]
        [SerializeField] private Text _namePlayerText;
        [SerializeField] private Text _levelPlayerText;

        [Space]

        [SerializeField] private GameObject _shopActiveObj;

        public Button PlayerInfoButton => _playerInfoButton;
        public Button FreeCoinsButtonButton => _freeCoinsButton;
        public Button SettingsButton => _settingsButton;
        public Button ShopButton => _shopButton;

        public Text LevelPlayerText => _levelPlayerText;
        public Text NamePlayerText => _namePlayerText;

        public GameObject ShopActiveObj => _shopActiveObj;

    }
}


